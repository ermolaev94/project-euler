# Problem #1: Multiples of 3 and 5

## Description

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

[Link](https://projecteuler.net/problem=1)


## Solution

The simplest solution is just iterate through the data:

```c++
int result = 0;
for (int i = 1; i < 1000; i++) {
    if (((i % 3) == 0) || ((i % 5) == 0)) {
        result += i;
    }
}
```

But this solution isn't optimal, trivial and not interesting. Use it for the check.

The key idea here is arithmetic sequence. We need to calculate arithmetic progression for the 3 and 5.
But there is a problem - some numbers cen be divided by both. So we need to do:
* Calc arithmetic sum for 3
* Calc arithmetic sum for 5
* Calc arithmetic sum for 15
* Sum = sum(3) + sum(5) - sum(15)

It's pretty easy to calculate arithmetic sum using the following equation:

```
sum = (a[1] + a[n]) * n / 2
```
