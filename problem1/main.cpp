#include <math.h>
#include <iostream>
#include <vector>

int main() {
    float n = 1000;

    std::vector<int> nums = {3, 5, 15};
    std::vector<int> coefs = {1, 1, -1};

    int sum = 0;

    for (size_t i = 0; i < nums.size(); i++) {
        int num = nums[i];
        float num_float = static_cast<float>(num);
        int num_count = static_cast<int>(std::floor((n - 1) / num_float));
        int max_num = num * num_count;
        int cur_sum = ((num + max_num) * num_count) / 2;
        sum += cur_sum * coefs[i];
    }

    std::cout << "Total sum: " << sum << std::endl;
}
