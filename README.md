# Project Euler repo

This repository is devoted to the [ProjectEuler.net](https://projecteuler.net/archives).

It contains problems from this site and solutions for them.

Each problem has its own folder with README file with the problem description and solutions description if it's necessary.
