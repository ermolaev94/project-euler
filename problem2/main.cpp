#include <iostream>

using namespace std;

int main()
{
    int max_n = static_cast<int>(1e+6 * 4);
    int n_6 = 0;
    int n_3 = 2;
    int n = 2;
    int sum = 0;

    while (n < max_n) {
        sum += n;

        n = 4 * n_3 + n_6;
        n_6 = n_3;
        n_3 = n;
    }

    std::cout << "Total sum: " << sum << std::endl;

    return 0;
}
